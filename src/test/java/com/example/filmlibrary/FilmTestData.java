package com.example.filmlibrary;


import com.example.filmlibrary.dbexample.dto.FilmDTO;
import com.example.filmlibrary.dbexample.model.Film;
import com.example.filmlibrary.dbexample.model.Genre;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface FilmTestData {
    FilmDTO FILM_DTO_1 = new FilmDTO(
            "Title1",
            20,
            "country",
            Genre.DRAMA,
            new HashSet<>(),
            new HashSet<>(),
            false
    );

    FilmDTO FILM_DTO_2 = new FilmDTO(
            "Title2",
            20,
            "country2",
            Genre.DRAMA,
            new HashSet<>(),
            new HashSet<>(),
            false
    );

    List<FilmDTO> FILM_DTO_LIST = Arrays.asList(FILM_DTO_1, FILM_DTO_2);


    Film FILM_1 = new Film(
            "Title2",
            20,
            "country2",
            Genre.DRAMA,
            new HashSet<>(),
            new HashSet<>()
    );

    Film FILM_2 = new Film(
            "Title3",
            20,
            "country2",
            Genre.DRAMA,
            new HashSet<>(),
            new HashSet<>()
    );


    List<Film> FILM_LIST = Arrays.asList(FILM_1, FILM_2);

    Set<FilmDTO> FILMS = new HashSet<>(com.example.filmlibrary.FilmTestData.FILM_DTO_LIST);
}
