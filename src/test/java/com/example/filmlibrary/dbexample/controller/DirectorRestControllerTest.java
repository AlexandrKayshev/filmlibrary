package com.example.filmlibrary.dbexample.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.example.filmlibrary.dbexample.dto.DirectorDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@Slf4j
public class DirectorRestControllerTest
        extends CommonTest {

    private static Long createdDirectorID;

    @Test
    @Order(0)
    protected void listAll() throws Exception {
        log.info("Тест по просмотра всех авторов через REST начат успешно");
        String result = mvc.perform(
                        get("/rest/directors/getAll")
                                .headers(headers)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0))))
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<DirectorDTO> directorDTOS = objectMapper.readValue(result, new TypeReference<List<DirectorDTO>>() {});
        directorDTOS.forEach(a -> log.info(a.toString()));
        log.info("Тест по просмотра всех авторов через REST закончен успешно");
    }

    @Test
    @Order(1)
    protected void createObject() throws Exception {
        log.info("Тест по созданию автора через REST начат успешно");
        //Создаем нового автора для создания через контроллер (тест дата)
        DirectorDTO directorDTO = new DirectorDTO("REST_TestDirectorFio",
                "2023-01-01", new HashSet<>(),
                false);


        DirectorDTO result = objectMapper.readValue(mvc.perform(post("/rest/directors/add")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .headers(super.headers)
                                .content(asJsonString(directorDTO))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                DirectorDTO.class);
        createdDirectorID = result.getId();
        log.info("Тест по созданию автора через REST закончен успешно " + result);

    }

    @Test
    @Order(2)
    protected void updateObject() throws Exception {
        log.info("Тест по обновления автора через REST начат успешно");
        //получаем нашего автора созданного (если запускать тесты подряд), если отдельно - создаем отдельную тест дату для апдейта
        DirectorDTO existingDirector = objectMapper.readValue(mvc.perform(get("/rest/directors/getOneById")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .headers(super.headers)
                                .param("id", String.valueOf(createdDirectorID))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                DirectorDTO.class);
        //обновляем поля
        existingDirector.setDirectorsFio("REST_TestDirectorFioUPDATED");


        //вызываем update через REST API
        mvc.perform(put("/rest/directors/update")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .headers(super.headers)
                        .content(asJsonString(existingDirector))
                        .param("id", String.valueOf(createdDirectorID))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по обновления автора через REST закончен успешно");
    }


    @Test
    @Order(4)
    protected void deleteObject() throws Exception {
        log.info("Тест по удалению автора через REST начат успешно");
        mvc.perform(delete("/rest/directors/delete/{id}", createdDirectorID)
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        DirectorDTO existingDirector = objectMapper.readValue(mvc.perform(get("/rest/directors/getOneById")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .headers(super.headers)
                                .param("id", String.valueOf(createdDirectorID))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                DirectorDTO.class);

        assertTrue(existingDirector.isDeleted());
        log.info("Тест по удалению автора через REST завершен успешно");

    }

}
