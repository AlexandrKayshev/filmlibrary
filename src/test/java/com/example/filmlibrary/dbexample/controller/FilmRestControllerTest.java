package com.example.filmlibrary.dbexample.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.example.filmlibrary.dbexample.dto.FilmDTO;
import com.example.filmlibrary.dbexample.model.Genre;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashSet;
import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@Slf4j
public class FilmRestControllerTest
        extends CommonTest {

    private static Long createdFilmID;

    @Test
    @Order(0)
    protected void listAll() throws Exception {
        log.info("Тест по просмотра всех авторов через REST начат успешно");
        String result = mvc.perform(
                        get("/rest/films/getAll")
                                .headers(headers)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect((ResultMatcher) jsonPath("$.*", hasSize(greaterThan(0))))
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<FilmDTO> filmDTOS = objectMapper.readValue(result, new TypeReference<List<FilmDTO>>() {});
        filmDTOS.forEach(a -> log.info(a.toString()));
        log.info("Тест по просмотра всех авторов через REST закончен успешно");
    }

    @Test
    @Order(1)
    protected void createObject() throws Exception {
        log.info("Тест по созданию автора через REST начат успешно");
        //Создаем нового автора для создания через контроллер (тест дата)

        FilmDTO filmDTO = new FilmDTO(
                "Title2",
                20,
                "country2",
                Genre.DRAMA,
                new HashSet<>(),
                new HashSet<>(),
                false
        );


        FilmDTO result = objectMapper.readValue(mvc.perform(post("/rest/films/add")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .headers(super.headers)
                                .content(asJsonString(filmDTO))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                FilmDTO.class);
        createdFilmID = result.getId();
        log.info("Тест по созданию автора через REST закончен успешно " + result);
        /*
        можно запустить один тест и по цепочке вызывать остальные:
        updateFilm(createdFilmID);
         */
    }

    @Test
    @Order(2)
    protected void updateObject() throws Exception {
        log.info("Тест по обновления автора через REST начат успешно");
        //получаем нашего автора созданного (если запускать тесты подряд), если отдельно - создаем отдельную тест дату для апдейта
        FilmDTO existingFilm = objectMapper.readValue(mvc.perform(get("/rest/films/getOneById")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .headers(super.headers)
                                .param("id", String.valueOf(createdFilmID))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                FilmDTO.class);
        //обновляем поля
        existingFilm.setFilmTitle("REST_TestFilmFioUPDATED");


        //вызываем update через REST API
        mvc.perform(put("/rest/films/update")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .headers(super.headers)
                        .content(asJsonString(existingFilm))
                        .param("id", String.valueOf(createdFilmID))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по обновления автора через REST закончен успешно");
    }


    @Test
    @Order(4)
    protected void deleteObject() throws Exception {
        log.info("Тест по удалению автора через REST начат успешно");
        mvc.perform(delete("/rest/films/delete/{id}", createdFilmID)
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        FilmDTO existingFilm = objectMapper.readValue(mvc.perform(get("/rest/films/getOneById")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .headers(super.headers)
                                .param("id", String.valueOf(createdFilmID))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                FilmDTO.class);

        assertTrue(existingFilm.isDeleted());
        log.info("Тест по удалению автора через REST завершен успешно");

    }

}

