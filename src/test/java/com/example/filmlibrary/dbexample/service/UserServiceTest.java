package com.example.filmlibrary.dbexample.service;

import com.example.filmlibrary.dbexample.dto.UserDTO;
import com.example.filmlibrary.dbexample.exception.MyDeleteException;
import com.example.filmlibrary.dbexample.model.User;
import org.junit.jupiter.api.Test;

public class UserServiceTest extends GenericTest<User, UserDTO> {
    
    @Test
    @Override
    protected void getAll() {
    
    }
    
    @Test
    @Override
    protected void getOne() {
    
    }
    
    @Test
    @Override
    protected void create() {
    
    }
    
    @Test
    @Override
    protected void update() {
    
    }
    
    @Test
    @Override
    protected void delete() throws MyDeleteException {
    
    }
    
    @Test
    @Override
    protected void restore() {
    
    }
    
    @Test
    @Override
    protected void getAllNotDeleted() {
    
    }
}
