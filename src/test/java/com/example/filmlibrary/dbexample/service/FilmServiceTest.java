package com.example.filmlibrary.dbexample.service;

import com.example.filmlibrary.FilmTestData;
import com.example.filmlibrary.dbexample.dto.FilmDTO;
import com.example.filmlibrary.dbexample.exception.MyDeleteException;
import com.example.filmlibrary.dbexample.mapper.FilmMapper;
import com.example.filmlibrary.dbexample.mapper.FilmWithDirectorsMapper;
import com.example.filmlibrary.dbexample.model.Film;
import com.example.filmlibrary.dbexample.repository.FilmRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class FilmServiceTest extends GenericTest<Film, FilmDTO> {

    FilmWithDirectorsMapper filmWithDirectorsMapper;

    public FilmServiceTest() {
        super();
        FilmService filmService = Mockito.mock(FilmService.class);
        repository = Mockito.mock(FilmRepository.class);
        mapper = Mockito.mock(FilmMapper.class);
        filmWithDirectorsMapper = Mockito.mock(FilmWithDirectorsMapper.class);
        service = new FilmService((FilmRepository) repository, (FilmMapper) mapper, filmWithDirectorsMapper);

    }

    @Test
    @Override
    protected void getAll() {

        Mockito.when(repository.findAll()).thenReturn(FilmTestData.FILM_LIST);
        Mockito.when(mapper.toDTOs(FilmTestData.FILM_LIST)).thenReturn(FilmTestData.FILM_DTO_LIST);
        List<FilmDTO> FilmDTOS = service.listAll();
        log.info("Testing getAll(): " + FilmDTOS);
        assertEquals(FilmTestData.FILM_LIST.size(), FilmDTOS.size());
    }

    @Test
    @Override
    protected void getOne() {

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(FilmTestData.FILM_1));
        Mockito.when(mapper.toDTO(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_DTO_1);
        FilmDTO filmDTO = service.getOne(1L);
        log.info("Testing getOne(): " + filmDTO);
        assertEquals(FilmTestData.FILM_DTO_1, filmDTO);
    }

    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(FilmTestData.FILM_DTO_1)).thenReturn(FilmTestData.FILM_1);
        Mockito.when(mapper.toDTO(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_DTO_1);
        Mockito.when(repository.save(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_1);
        FilmDTO authorDTO = service.create(FilmTestData.FILM_DTO_1);
        log.info("Testing create(): " + authorDTO);
        assertEquals(FilmTestData.FILM_DTO_1, authorDTO);

    }

    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(FilmTestData.FILM_DTO_1)).thenReturn(FilmTestData.FILM_1);
        Mockito.when(mapper.toDTO(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_DTO_1);
        Mockito.when(repository.save(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_1);
        FilmDTO authorDTO = service.update(FilmTestData.FILM_DTO_1);
        log.info("Testing update(): " + authorDTO);
        assertEquals(FilmTestData.FILM_DTO_1, authorDTO);

    }

    @Test
    @Override
    protected void delete() throws MyDeleteException {

        Mockito.when(((FilmRepository) repository).checkFilmForDeletion(1L)).thenReturn(true);
//        Mockito.when(authorRepository.checkFilmForDeletion(2L)).thenReturn(false);
        Mockito.when(repository.save(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_1);
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(FilmTestData.FILM_1));
        log.info("Testing delete() before: " + FilmTestData.FILM_1.isDeleted());
        service.delete(1L);
        log.info("Testing delete() after: " + FilmTestData.FILM_1.isDeleted());
        assertTrue(FilmTestData.FILM_1.isDeleted());
    }

    @Test
    @Override
    protected void restore() {

        FilmTestData.FILM_2.setDeleted(true);
        Mockito.when(repository.save(FilmTestData.FILM_2)).thenReturn(FilmTestData.FILM_2);
        Mockito.when(repository.findById(3L)).thenReturn(Optional.of(FilmTestData.FILM_2));
        log.info("Testing restore() before: " + FilmTestData.FILM_2.isDeleted());
        ((FilmService) service).restore(3L);
        log.info("Testing restore() after: " + FilmTestData.FILM_2.isDeleted());
        assertFalse(FilmTestData.FILM_2.isDeleted());
    }

    @Test
    @Override
    protected void getAllNotDeleted() {

        FilmTestData.FILM_2.setDeleted(true);
        List<Film> authors = FilmTestData.FILM_LIST.stream().filter(Predicate.not(Film::isDeleted)).toList();
        Mockito.when(repository.findAllByIsDeletedFalse()).thenReturn(authors);
        Mockito.when(mapper.toDTOs(authors)).thenReturn(
                FilmTestData.FILM_DTO_LIST.stream().filter(Predicate.not(FilmDTO::isDeleted)).toList());
        List<FilmDTO> authorDTOS = service.listAllNotDeleted();
        log.info("Testing getAllNotDeleted(): " + authorDTOS);
        assertEquals(authors.size(), authorDTOS.size());
    }



}

