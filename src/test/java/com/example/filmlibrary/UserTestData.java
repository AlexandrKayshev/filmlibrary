package com.example.filmlibrary;


import com.example.filmlibrary.dbexample.dto.RoleDTO;
import com.example.filmlibrary.dbexample.dto.UserDTO;
import com.example.filmlibrary.dbexample.model.Role;
import com.example.filmlibrary.dbexample.model.User;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;

public interface UserTestData {

    UserDTO USER_DTO = new UserDTO(
            "login",
            "password",
            "firstName",
            "lastName",
            "middle_name",
            "birthDate",
            "phone",
            "address",
            "email",
            LocalDate.now(),
            new RoleDTO(),
            new HashSet<>(),
            "changePasswordToken",
            false
    );


    List<UserDTO> USER_DTO_LIST = List.of(USER_DTO);

    User USER = new User(
            "login",
            "password",
            "firstName",
            "lastName",
            "middleName",
            LocalDate.now(),
            "phone",
            "address",
            "email",
            LocalDate.now(),
            new Role(),
            new HashSet<>(),
            "changePasswordToken"
    );










}
