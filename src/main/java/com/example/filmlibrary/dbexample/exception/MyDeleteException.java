package com.example.filmlibrary.dbexample.exception;

public class MyDeleteException extends Exception {
    public MyDeleteException(String message) {
        super(message);
    }
}
