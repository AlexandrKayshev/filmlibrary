package com.example.filmlibrary.dbexample.rest;

import com.example.filmlibrary.dbexample.dto.DirectorDTO;
import com.example.filmlibrary.dbexample.dto.FilmDTO;
import com.example.filmlibrary.dbexample.model.Director;
import com.example.filmlibrary.dbexample.model.Film;
import com.example.filmlibrary.dbexample.service.DirectorService;
import com.example.filmlibrary.dbexample.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/films")
@Tag(name = "Films", description = "Controller for working with movies")
public class FilmController extends GenericController<Film, FilmDTO> {

    private final FilmService filmService;
    private final DirectorService directorService;

    public FilmController(FilmService filmService, DirectorService directorService) {
        super(filmService);
        this.filmService = filmService;
        this.directorService = directorService;
    }

    @Operation(description = "Add a director to a movie", method = "addDirector")
    @RequestMapping(value = "/addDirector", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDTO> addDirector(@RequestParam(value = "filmId") Long filmId,
                                               @RequestParam(value = "directorId") Long directorId) {
        FilmDTO filmDTO = filmService.getOne(filmId);
        DirectorDTO directorDTO = directorService.getOne(directorId);
        filmDTO.getDirectorsIds().add(directorDTO.getId());
        return ResponseEntity.status(HttpStatus.OK).body(filmService.update(filmDTO));
    }
}



