package com.example.filmlibrary.dbexample.rest;

import com.example.filmlibrary.dbexample.dto.FilmDTO;
import com.example.filmlibrary.dbexample.dto.OrderDTO;
import com.example.filmlibrary.dbexample.dto.UserDTO;
import com.example.filmlibrary.dbexample.model.Order;
import com.example.filmlibrary.dbexample.service.FilmService;
import com.example.filmlibrary.dbexample.service.OrderService;
import com.example.filmlibrary.dbexample.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/orders")
@Tag(name = "Orders", description = "Controller for working with orders")
public class OrderController extends GenericController<Order, OrderDTO> {

    private final OrderService orderService;
    private final FilmService filmService;
    private final UserService userService;

    public OrderController(OrderService orderService, FilmService filmService, UserService userService) {
        super(orderService);
        this.orderService = orderService;
        this.filmService = filmService;
        this.userService = userService;
    }

    @Operation(description = "Rent a movie", method = "create")
    @RequestMapping(value = "/addOrder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> create(@RequestBody OrderDTO newEntity) {
        FilmDTO filmDTO = filmService.getOne(newEntity.getFilmId());
        UserDTO userDTO = userService.getOne(newEntity.getUserId());
        filmDTO.getOrdersIds().add(newEntity.getId());
        userDTO.getOrdersIds().add(newEntity.getId());

        return ResponseEntity.status(HttpStatus.CREATED).body(orderService.create(newEntity));
    }
}

