package com.example.filmlibrary.dbexample.constants;

public interface UserRoleConstants {
    String ADMIN = "ADMIN";
    String LIBRARIAN = "LIBRARIAN";
    String USER = "USER";
}
