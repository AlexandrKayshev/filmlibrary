package com.example.filmlibrary.dbexample.dto;

import com.example.filmlibrary.dbexample.model.Film;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmWithDirectorsDTO extends FilmDTO {
    private Set<DirectorDTO> directors;
    
    public FilmWithDirectorsDTO(Film film, Set<DirectorDTO> directors) {
        super(film);
        this.directors = directors;
    }
}
