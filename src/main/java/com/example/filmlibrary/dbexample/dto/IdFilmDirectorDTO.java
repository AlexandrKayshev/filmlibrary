package com.example.filmlibrary.dbexample.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class IdFilmDirectorDTO {
    private Long filmId;
    private Long directorId;
}
