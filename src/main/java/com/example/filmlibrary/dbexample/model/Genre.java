package com.example.filmlibrary.dbexample.model;

public enum Genre {
    FANTASY("Fantasy"),
    COMEDY("Comedy"),
    DRAMA("Drama"),
    MELODRAMA("Melodrama"),
    THRILLER("Thriller"),
    HORROR("Horror"),
    ADVENTURES("Adventures"),
    BLOCKBUSTER("Blockbuster"),
    MULTIPLICATION("Multiplication");
    private final String genreTextDisplay;
    
    Genre(String genreName) {
        this.genreTextDisplay = genreName;
    }
    
    public String getGenreTextDisplay() {
        return this.genreTextDisplay;
    }
}
